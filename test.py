from openerp.osv import fields, osv


class test_module(osv.osv):
    _name = "test.sale"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True),
        'title': fields.char('Title', size=64, required=True, translate=True)
    }
